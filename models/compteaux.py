# -*- coding: utf-8 -*-

from odoo import models, fields, api


# variables to be changed according to your configuration

class Compteaux(models.Model):
    """This function does something.

        Args:
           name (str):  The name to use.

        Kwargs:
           state (bool): Current state to be in.

        Returns:
           int.  The return code::

              0 -- Success!
              1 -- No good.
              2 -- Try again.

        Raises:
           AttributeError, KeyError

        A really great idea.  A way you might use me is

        >>> print (name='foo', state=None)
        0

        BTW, this always returns 0.  **NEVER** use with :class:`MyPublicClass`.

        """
    _name = 'teos.compteaux'

    x_aux = fields.Char(string='code complete aux', size=24)
    cod_empresa = fields.Char(string='code entreprise', size=10)
    cod_pgc = fields.Char(string='code plan comptable', size=5)
    x_subcta = fields.Char(string='code sous compte', size=16)
    cod_grp_cod = fields.Char(string='code de groupe de code', size=5)
    cod_aux = fields.Char(string='code auxiliere', size=8)
    des_aux = fields.Char(string='denomination du compte', size=100)
    ck_ext_aux = fields.Char(string='detail au extrait par auxiliaire', size=1, default='N')
    cod_estacionalidad = fields.Char(string='code saisonnalite', size=10)
    cod_incr_presup = fields.Char(string='code augmentation budgetaire', size=10)
    cod_aux_regulariz = fields.Char(string='code compte cloture', size=8)

    def get_accounts_informations(self):

        oracle_conn = self.env.ref('	teos.teos_oracle_connector')
        db_source = self.env['base.external.dbsource'].browse(oracle_conn.id)
        resultat = db_source.execute_cx_Oracle(""" SELECT * FROM EMP """, '', '')[0]
        for account_info in resultat:
            existing_accounts = self.env['teos.compteaux'].search([('x_aux', '=', account_info[0])])

            if not existing_accounts:
                self.env['teos.compteaux'].create({'x_aux': account_info[0],
                                                   'cod_empresa': account_info[1],
                                                   'cod_pgc': account_info[2],
                                                   'x_subcta': account_info[3],
                                                   'cod_grp_cod': account_info[4],
                                                   'cod_aux': account_info[5],
                                                   'des_aux': account_info[6],
                                                   'ck_ext_aux': account_info[7],
                                                   'cod_estacionalidad': account_info[8],
                                                   'cod_incr_presup': account_info[9],
                                                   'cod_aux_regulariz': account_info[10]

                                                   })

    @api.model
    def _import_accounts(self):
        return self.get_accounts_informations()
