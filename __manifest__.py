# -*- coding: utf-8 -*-
{
    'name': "TEOS",

    'summary': """Manage trainings""",

    'description': """
        Recuperation  des donnees Oracle
    """,

    'author': "Salma",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'base_external_dbsource_oracle'
    ],

    # always loaded
    'data': [
        'data/automated_actions.xml',
        'data/oracle_connection.xml',
        # 'security/ir.model.access.csv',
        'views/teos_menuitem.xml',
        'views/compteaux.xml',
        'views/pmouvement.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo.xml',
    # ],
}
