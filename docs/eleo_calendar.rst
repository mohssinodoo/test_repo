Eleo Calendar (eleo\_calendar)
==============================


.. toctree::

    eleo_calendar.controllers
    eleo_calendar.models
    eleo_calendar.wizard

Module contents
---------------

.. automodule:: eleo_calendar
    :members:
    :undoc-members:
    :show-inheritance:
