mydocs.models package
=====================

Submodules
----------

mydocs.models.http\_request module
----------------------------------

.. automodule:: mydocs.models.http_request
    :members:
    :undoc-members:
    :show-inheritance:

mydocs.models.res\_partner module
---------------------------------

.. automodule:: mydocs.models.res_partner
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mydocs.models
    :members:
    :undoc-members:
    :show-inheritance:
