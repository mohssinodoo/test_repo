Welcome to TestoDocs's documentation!
=====================================

.. toctree::
   test_repo.models
   eleo_calendar
   licence
   help



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
